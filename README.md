## 22blacklist

Detect known illegal sites that often get posted on online forums.

### Usage

`checkBlacklist` will return either `null` if no match was found or an array like this:
```php
$match = [
    // The blacklisted domain that was found
    'domain' => 'gloyah.net',
    
    // The regex that found the domain
    'regex' => '/(https?:\/\/|[^\w]|^)gloyah\.net(\/[\w]+)?(?:[^\w]|$)/mi',
    
    // The text you passed in
    'text' => 'This will trigger https://gloyah.net/asdf the blacklist',
    
    // The exact match
    'match' => 'https://gloyah.net/asdf',
    
    // A pre-formatted log entry
    'fmt' => "192.168.2.1 tried to post blacklisted domain \"gloyah.net\"\n- Match: https://gloyah.net/asdf\n- Regex: /(https?:\/\/|[^\w]|^)gloyah\.net(\/[\w]+)?(?:[^\w]|$)/mi"
]
```

**Example Usage**
```php
include 'post_blacklist.php';

if (!empty($_POST['body'])) {
    $match = checkBlacklist($_POST['body']);
    if ($match) {
        syslog(LOG_INFO, $match['fmt']);
        die('You tried to post a blacklisted domain!');
    }
}
```