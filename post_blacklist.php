<?php
require __DIR__ . '/blacklist.php';

define('BLACKLISTED_DOMAINS', array_merge(RAW_BLACKLIST, [
// Add custom blacklisted sites here
    'pornhub.com' // <- example
]));

function checkBlacklist($text, $agressiveSearch = false)
{
    $preg_check = function($regex, $string) {
        return preg_replace($regex, '', $string) !== $string;
    };
    
    foreach(BLACKLISTED_DOMAINS as $domain) {
        if ($agressiveSearch) {
            $rgx = '/(https?:\/\/|[^\w]|^)?' . preg_quote($domain) . '(\/[\w]+)?(?:[^\w]|$)?/mi';
        } else {
            $rgx = '/(https?:\/\/|[^\w]|^)' . preg_quote($domain) . '(\/[\w]+)?(?:[^\w]|$)/mi';
        }
        if ($preg_check($rgx, $text)) {
            preg_match($rgx, $text, $matches);
            $match = empty($matches) ? 'N/A' : $matches[0];
            
            return [
                'domain' => $domain,
                'regex' => $rgx,
                'text' => $text,
                'match' => $match,
                'fmt' => sprintf("%s tried to post blacklisted domain \"%s\"\n- Match: %s\n- Regex: %s", $_SERVER['REMOTE_ADDR'], $domain, $match, $rgx)
            ];
        }
    }
    return null;
}